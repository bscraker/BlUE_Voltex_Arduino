#include "Engine.h"
    
namespace BlUE_Voltex
{
    Engine::Engine()
    {
        datas = "";

        pinMode(2, OUTPUT);
        pinMode(3, OUTPUT);
        pinMode(4, OUTPUT);
        pinMode(5, OUTPUT);
        pinMode(6, OUTPUT);
        pinMode(7, OUTPUT);
  
        pinMode(9, INPUT_PULLUP);
  
        Serial.begin(9600);
    
        ChangeLED(0, 0, 0, 0);
        ChangeLED(1, 0, 0, 0);
    }

    void Engine::Loop()
    {
        while (true)
        {
            ReadData();
            WriteData();

            delay(1);
        }
    }

    void Engine::ChangeLED(bool index,
                           unsigned char r, unsigned char g, unsigned char b)
    {
        unsigned char color[] = {b, g, r};

        index = 1 - index;
  
        for (int i = 0; i < 3; ++i)
        {
            analogWrite((index * 3) + 2 + i, color[i]);
        }
    }

    String Engine::ParseData(String* data)
    {
        int index = data->indexOf(" ");

        String _data = "";

        if (index > 0)
        {
          _data = data->substring(0, index);
          *data = data->substring(index + 1);
        }

        return _data;
    }

    void Engine::ReadData()
    {
        while (Serial.available())
        {
            datas.concat((char)Serial.read());
        }

        int lastIndex2 = datas.lastIndexOf('|');

        if (lastIndex2 != -1)
        {
            int lastIndex = datas.lastIndexOf('!', lastIndex2 - 1);

            if (lastIndex != -1 && lastIndex < lastIndex2)
            {
                String currentData = datas.substring(lastIndex + 1, lastIndex2);

                datas = datas.substring(lastIndex2 + 1);

                unsigned char r = atoi(ParseData(&currentData).c_str());
                unsigned char g = atoi(ParseData(&currentData).c_str());
                unsigned char b = atoi(ParseData(&currentData).c_str());

                ChangeLED(0, r, g, b);

                r = atoi(ParseData(&currentData).c_str());
                g = atoi(ParseData(&currentData).c_str());
                b = atoi(ParseData(&currentData).c_str());
       
                ChangeLED(1, r, g, b);
            }
        }
    }

    void Engine::WriteData()
    {
        String data = "!";

        char value[5];

        itoa(analogRead(A0), value, 10);
        data += value;
        data += " ";

        itoa(analogRead(A1), value, 10);
        data += value;
        data += " ";

        itoa(analogRead(A2), value, 10);
        data += value;
        data += " ";

        itoa(analogRead(A3), value, 10);
        data += value;
        data += " ";

        itoa(digitalRead(8), value, 10);
        data += value;
        data += " ";

        itoa(1 - digitalRead(9), value, 10);
        data += value;
        data += " ";

        data += "|";

        Serial.print(data);
    }
}