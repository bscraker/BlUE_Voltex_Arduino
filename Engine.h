#pragma once

#include "Arduino.h"

namespace BlUE_Voltex
{
    class Engine
    {
    public:
        Engine();

        void Loop();

    private:
        String datas;

        void ChangeLED(bool index,
                       unsigned char r, unsigned char g, unsigned char b);

        String ParseData(String* data);

        void ReadData();
        void WriteData();
    };
}